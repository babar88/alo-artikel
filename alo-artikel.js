import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `alo-artikel`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class AloArtikel extends PolymerElement {
  static get template(){
    return html`
      <style>
        .wrapper{font-family: 'Lato';font-size:14px;padding:16px;max-width: 480px;margin:auto;}
        h1{font-size:18px;border-radius:4px;padding:6px;margin:6px;
        -webkit-box-shadow: 0 0 10px 0 #BFBFBF;
        box-shadow: 0 0 10px 0 #BFBFBF;}
        .content{border-radius:4px;padding:6px;margin:6px;
        -webkit-box-shadow: 0 0 10px 0 #BFBFBF;
        box-shadow: 0 0 10px 0 #BFBFBF;text-align:left;}
        p img{max-width:100%;height:auto;}
      </style>
      <div class="wrapper">
        <h1>[[aloTitle]]</h1>
        <p class="content" inner-h-t-m-l="[[aloContent]]"></p>
      </div>
    `;
  }
  
  static get properties() {
    return {
      aloTitle: String,
      aloContent: String
    };
  }

  ready(){
    super.ready();
    this.aloContent = JSON.parse(this.aloContent);
  }
}

window.customElements.define('alo-artikel', AloArtikel);
